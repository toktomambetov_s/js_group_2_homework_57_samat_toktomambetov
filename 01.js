const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

let task1 = tasks.filter(task => {
   return task.category === 'Frontend';
}).reduce((acc, task) => acc + task.timeSpent, 0);

let task2 = tasks.filter(task => {
   return task.type === 'bug';
}).reduce((acc, task) => acc + task.timeSpent, 0);

let task3 = tasks.filter(task => {
   return task.title.includes('UI');
}).length;

let task4 = tasks.filter(task => {
   return task.category;
}).reduce((acc, task) => {
    if (task.category === 'Frontend') {
        acc.Frontend++;
    }

    if (task.category === 'Backend') {
        acc.Backend++;
    }

    return acc;
}, {Frontend: 0, Backend: 0});

let task5 = tasks.filter(task => {
   return task.timeSpent > 4;
}).map(task => {
    return {
        title: task.title,
        category: task.category,
    }
});

console.log('Time spent in task category "Frontend": ' + task1);
console.log('Time spent in task type "bug": ' + task2);
console.log('Amount of tasks that have "UI" in their names: ' + task3);
console.log(task4);
console.log(task5);