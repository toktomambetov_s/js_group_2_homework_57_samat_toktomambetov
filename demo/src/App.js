import React, { Component } from 'react';
import SpentListBuilder from "./containers/SpentListBuilder/SpentListBuilder";

class App extends Component {
  render() {
    return (
        <SpentListBuilder/>
    );
  }
}

export default App;
