import React from 'react';
import './Item.css';

const Item = props => {
    return (
        <div className="Item">
            <span>{props.name} </span>
            <span>{props.cost} KGS</span>
            <button className="Delete" onClick={props.remove}>Delete</button>
        </div>
    )
};

export default Item;