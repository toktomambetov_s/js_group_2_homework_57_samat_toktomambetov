import React from 'react';
import Wrapper from "../../hoc/Wrapper";
import Item from "../Item/Item";
import './SpentList.css';

const SpentList = props => {
    const calcTotalSpent = props.expenses.reduce((totalCost, expense) => {
       return totalCost + parseInt(expense.cost);
    }, 0);

    return (
        <Wrapper>
            <div className="List">
                {props.expenses.map((expense, index) => {
                    return <Item key={index} name={expense.name} cost={expense.cost} remove={() => props.remove(index)}/>
                })}
                <div className="Total">Total spent: {calcTotalSpent}</div>
            </div>

        </Wrapper>
    )
};

export default SpentList;