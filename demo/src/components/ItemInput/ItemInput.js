import React from 'react';
import './ItemInput.css';

const ItemInput = props => {
    return (
        <div>
            <label htmlFor="">Item name: </label>
            <input className="ItemName" type="text" onChange={props.itemName} value={props.name}/>
            <label htmlFor="">Cost: </label>
            <input className="Cost" type="number" onChange={props.itemCost} value={props.cost}/>
            <button className="Add" onClick={props.addItem}>Add</button>
        </div>
    )
};

export default ItemInput;