import React, { Component } from 'react'
import Wrapper from "../../hoc/Wrapper";
import ItemInput from "../../components/ItemInput/ItemInput";
import SpentList from "../../components/SpentList/SpentList";
import './SpentListBuilder.css';

class SpentListBuilder extends Component {
    state = {
        expenses: [],
        name: '',
        cost: '',
    };

    changeExpenseName = e => this.setState({name: e.target.value});
    changeExpenseCost = e => this.setState({cost: e.target.value});

    addItem = () => {
      const expenses = [...this.state.expenses];
      expenses.push({name: this.state.name, cost: this.state.cost});
      this.setState({expenses, name: " ", cost: " "});
    };

    removeItem = index => {
      const expenses = [...this.state.expenses];
      expenses.splice(index, 1);
      this.setState({expenses});
    };

    render() {
        return (
            <Wrapper>
                <div className="container">
                    <ItemInput itemName={this.changeExpenseName} itemCost={this.changeExpenseCost} addItem={this.addItem} name={this.state.name} cost={this.state.cost}/>
                    <SpentList expenses={this.state.expenses} remove={this.removeItem}/>
                </div>
            </Wrapper>
        )
    }
}

export default SpentListBuilder;